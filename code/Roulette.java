import java.util.Scanner;
public class Roulette {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome to Roulette! Let's start the game.");
        RouletteWheel rw = new RouletteWheel();
        int money = 1000;
        int betMoney = 0;
        int betNum = 0;
        int moneyWon = 0;
        int numRolled = 0;
        int gamesPlayed = 0;
        boolean keepPlaying = true;
        boolean proceed = false;
        String answer = "";
        while(keepPlaying) {
            proceed = false;
            gamesPlayed++;
            while(!proceed) {
                System.out.println("How much do you want to bet?");
                betMoney = sc.nextInt();
                if(betMoney < 1 || betMoney > money) {
                    System.out.println("You can't bet this much!");
                } else {
                    System.out.println("What number do you want to bet on?");
                    betNum = sc.nextInt();
                    if(betNum < 0 || betNum > 36) {
                        System.out.println("This isn't a valid number!");
                    } else {
                        proceed = true;
                    }
                }
            }

            rw.spin();
            numRolled = rw.getValue();
            moneyWon = moneyWon(betNum, betMoney, numRolled);
            if(moneyWon < 0) {
                System.out.println("Oh no! You bet on the number "+betNum+", but a "+numRolled+" was rolled! You lost "+betMoney+"$");
            } else {
                System.out.println("Congradulations! You bet on the number "+betNum+", and a "+numRolled+" was rolled! You won "+moneyWon+"$");
            }
            money = money+moneyWon;
            if (money < 1) {
                System.out.println("You ran out of money! Better luck next time!");
                keepPlaying = false;
            } else {
                System.out.println("you have "+money+" money. Do you want to continue? (y/n)");
                answer = sc.nextLine();
                answer = sc.nextLine();
                if(answer.equals("n")) {
                    System.out.println("Exiting Roulette.exe");
                    keepPlaying = false;
                } else {
                    System.out.println("You have played "+gamesPlayed+" times. Going in for another...");
                }
            }
        }
        System.out.println("Thank you for playing Roulette! You spun the wheel a total of "+gamesPlayed+" times, and ended with "+money+" money!");
    
    }

    public static int moneyWon(int betNum, int betMoney, int numRolled) {
        int winnings = 0;
        if(betNum == numRolled) {
            winnings = betMoney*35;
        } else {
            winnings = winnings-betMoney;
        }
        return winnings;
    }
}