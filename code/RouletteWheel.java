import java.util.Random;
public class RouletteWheel {
   private  Random rand = new Random();
   private int numSpin = 0;
   
   public RouletteWheel() {
        this.numSpin = rand.nextInt(37);
   }

   public void spin() {
        this.numSpin = rand.nextInt(37);
   }

   public int getValue() {
        return this.numSpin;
   }
}